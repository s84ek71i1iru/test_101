<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    const ASSERT_TYPE_INTEGER = 'integer';

    const ASSERT_TYPE_STRING = 'string';

    /**
     * @var array
     */
    protected $expectedResponseStructure = [
        'link',
        'method',
        'message',
        'code',
        'items'
    ];

    /**
     * @param TestResponse $response
     */
    protected function jsonResponseDefaultAssertion(TestResponse $response)
    {
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure($this->expectedResponseStructure);
    }

    /**
     * @param TestResponse $response
     * @param array $itemStructures
     */
    protected function assertResponseItems(TestResponse $response, array $itemStructures)
    {
        $items = $response->json()['items'];
        foreach ($items as $item) {
            $this->assertItemStructure($item, $itemStructures);
        }
    }

    /**
     * @param TestResponse $response
     * @param array $itemStructures
     */
    protected function assertResponseItem(TestResponse $response, array $itemStructures)
    {
        $this->assertItemStructure(
            $response->json()['items'],
            $itemStructures
        );
    }

    /**
     * @param array $item
     * @param array $structures
     */
    protected function assertItemStructure(array $item, array $structures)
    {
        foreach ($structures as $structure) {
            $this->assertArrayHasKey($structure['field'], $item);
            $this->assertInternalType($structure['type'], $item[$structure['field']]);
        }
    }
}
