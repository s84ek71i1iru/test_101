<?php

namespace Tests\Feature;

use App\Services\UserService;
use App\Transformers\Users\Show;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Mockery as m;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }

    /**
     * @test
     * @group user
     */
    public function 依照使用者編號取得使用者資料()
    {
        # Arrange
        $user = factory(User::class)->create();
        $service = m::mock(UserService::class);
        $this->app->instance(UserService::class, $service);
        $service->shouldReceive('fetchedByID')->once();

        $transformer = m::mock(Show::class);
        $this->app->instance(Show::class, $transformer);
        $transformer->shouldReceive('transform')
            ->once()
            ->andReturn(new User([
                'name' => 'DL',
                'email' => 'dl@fg.com.tw',
            ]));

        # Act
        $response = $this->get(route('users.show', ['user' => $user->id]));

        # Assert
        $this->jsonResponseDefaultAssertion($response);
        $this->assertResponseItem($response, [
            [
                'field' => 'name',
                'type' => self::ASSERT_TYPE_STRING
            ],
            [
                'field' => 'email',
                'type' => self::ASSERT_TYPE_STRING
            ]
        ]);
    }
}
