<?php

namespace Tests\Unit;

use App\Repositories\UserRepository;
use App\Services\UserService;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery as m;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }

    /**
     * @test
     * @group userService
     */
    public function it_should_get_user_by_id()
    {
        # Arrange
        $user = factory(User::class)->create();

        $repository = m::mock(UserRepository::class);
        $this->app->instance(UserRepository::class, $repository);
        $repository->shouldReceive('getActiveUserByID')
            ->once()
            ->andReturn($user);

        # Act
        $fetchedUser = app(UserService::class)->fetchedByID($user->id);

        # Assert
        $this->assertEquals($user->id, $fetchedUser->id);
    }

    /**
     * @test
     * @group userRepository
     */
    public function it_should_get_active_by_id()
    {
        # Arrange
        $user = factory(User::class)->create(['status' => 1]);

        # Act
        $fetchedUser = app(UserRepository::class)->getActiveUserByID($user->id);

        # Assert
        $this->assertTrue($fetchedUser->exists());
    }

    /**
     * @test
     * @group userRepository
     */
    public function it_should_get_empty_active_user_by_id()
    {
        # Arrange
        $user = factory(User::class)->create(['status' => 2]);

        # Act
        $fetchedResult = app(UserRepository::class)->getActiveUserByID($user->id);

        # Assert
        $this->assertNull($fetchedResult);
    }
}
