<?php

namespace App\Http\Controllers\Api;

use App\Formatters\Custom\Success;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Transformers\Users\Show;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
    }

    /**
     * @param User $user
     * @param UserService $service
     * @param Show $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(User $user, UserService $service, Show $transformer)
    {
        $user = $service->fetchedByID($user->id);
        return response()->json(
            (new Success('依照會員編號取得會員資料'))->format(request(), $transformer->transform($user))
        );
    }
}
