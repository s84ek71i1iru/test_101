<?php

namespace App\Transformers\Users;

use Yish\Generators\Foundation\Transform\TransformContract;

class Show implements TransformContract
{
    public function transform($user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email
        ];
    }
}
