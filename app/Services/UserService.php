<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\User;
use Yish\Generators\Foundation\Service\Service;

class UserService extends Service
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userID
     * @return User
     */
    public function fetchedByID(int $userID): User
    {
        return $this->repository->getActiveUserByID($userID);
    }
}
