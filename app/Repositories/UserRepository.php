<?php

namespace App\Repositories;

use App\User;
use Yish\Generators\Foundation\Repository\Repository;

class UserRepository extends Repository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $userID
     * @return mixed
     */
    public function getActiveUserByID(int $userID)
    {
        return $this->model
            ->where([
                'id' => $userID,
                'status' => 1
            ])->first();
    }
}
